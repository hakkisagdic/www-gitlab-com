---
layout: handbook-page-toc
title: "Working with a new team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Working with a new team

A new team can take many forms:
1. You (the manager) are new to a team that is already in place
2. Your existing team picks up 1 or more new team members
3. Your team becomes smaller and team members have to pick up more tasks

In any of these cases, learning is really important.  You all need to learn about each other and how you will work together.  You all need to learn about the work you need to do as a team.  There may be one team member who knows a lot.  How do you get that knowledge shared?

Here are some ideas to help you and/or your team get up to speed:

### Rotating Team work
Something to try on a team where 50% may be new and 1 or 2 people have knowledge you want to share.
Try setting up a rotation where team members work together on an issue.  1 of the pair needs to have good knowledge of what to do and the other is the learner.  Every 2-3 days, the pair has to rotate.  The team member who has been on the issue the longest has to go to a new issue and the remaining team member in the pair has to teach the new team member joining the issue what is being done.  This can be really uncomfortable at first and may slow things down, but it will help force knowledge sharing.

### Shadow people for part of a workday
Have new team members setup time slots to shadow other team members or stable-counterparts for part of a day.  The mentor/shadowee will need to be more vocal in explaining what they are doing.  
